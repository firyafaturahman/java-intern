package com.javaintern;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        // variabel & tipe data
        String nama = "Firya Faturahman";
        System.out.println("Cetak variabel nama bertipe string: " + nama);

        char cnama = 'F';
        System.out.println("Cetak variabel cnama bertipe char: " + cnama);

        byte angka1 = 8;
        System.out.println("Cetak variabel angka1 bertipe byte: " + angka1);

        short angka2 = 16;
        System.out.println("Cetak variabel angka2 bertipe short: " + angka2);

        int angka3 = 32;
        System.out.println("Cetak variabel angka3 bertipe int: " + angka3);

        long angka4 = 64L;
        System.out.println("Cetak variabel angka4 bertipe long: " + angka4);

        float angka5 = 64.8f;
        System.out.println("Cetak variabel angka5 bertipe float: " + angka5);

        double angka6 = 64.67;
        System.out.println("Cetak variabel angka6 bertipe double: " + angka6);

        boolean isMarried = false;
        System.out.println("Cetak variabel isMarried bertipe boolen: " + isMarried);

        String[] buah = new String[5];
        buah[0] = "jeruk";
        buah[1] = "apel";
        buah[2] = "salak";
        buah[3] = "rambutan";
        buah[4] = "kecapi";

        for (String item : buah) {
            System.out.println(item);
        }
    }
}
