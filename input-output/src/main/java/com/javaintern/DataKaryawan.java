package com.javaintern;

import java.util.Scanner;

public class DataKaryawan {
  public static void main(String[] args) {
    // properties
    String nama;
    String alamat;
    boolean statusPernikahan;
    int usia;
    int gaji;

    // input
    Scanner scanner = new Scanner(System.in);
    System.out.println("Nama karyawan: ");
    nama = scanner.nextLine();

    System.out.println("Alamat karyawan: ");
    alamat = scanner.nextLine();

    System.out.println("Status pernikahan: ");
    statusPernikahan = scanner.nextBoolean();

    System.out.println("Usia karyawan: ");
    usia = scanner.nextInt();

    System.out.println("Gaji karyawan: ");
    gaji = scanner.nextInt();

    // output
    System.out.println("===================");
    System.out.println("Nama karyawan: " + nama);
    System.out.println("Alamat karyawan: " + alamat);
    if (statusPernikahan == true) {
      String status = "Menikah";
      System.out.println("Status pernikahan: " + status);
    } else {
      String status = "Belum menikah";
      System.out.println("Status pernikahan: " + status);
    }
    System.out.println("Usia karyawan: " + usia + " tahun");
    System.out.println("Gaji karyawan: Rp " + gaji + " rupiah");
  }
}
