package com.pbo;

public class Mobil extends Keterangan {

  // properties
  private String merk;
  private int roda;
  private double besarSilinder;

  public Mobil(String merk, int roda, double besarSilinder) {
    this.merk = merk;
    this.roda = roda;
    this.besarSilinder = besarSilinder;
  }

  public Mobil() {
  }

  public String getMerk() {
    return merk;
  }

  public void setMerk(String merk) {
    this.merk = merk;
  }

  public int getRoda() {
    return roda;
  }

  public void setRoda(int roda) {
    this.roda = roda;
  }

  public double getBesarSilinder() {
    return besarSilinder;
  }

  public void setBesarSilinder(double besarSilinder) {
    this.besarSilinder = besarSilinder;
  }

  @Override
  public String tipe(String tipe) {
    return "Mobil bertipe: " + tipe;
  }

  @Override
  public String tahunPembuatan(String tahun) {
    return "Mobil dibuat pada tahun: " + tahun;
  }
}
