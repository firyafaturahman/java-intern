package com.pbo;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        Mobil objMobil = new Mobil();
        objMobil.setMerk("Mazda");
        objMobil.setRoda(4);
        objMobil.setBesarSilinder(2000);

        System.out.println("Merk mobil: " + objMobil.getMerk());
        System.out.println("Roda mobil: " + objMobil.getRoda());
        System.out.println("Kecepatan mobil: " + objMobil.getBesarSilinder());

        MobilAvanza mobilAvanza = new MobilAvanza();
        mobilAvanza.setRoda(4);
        mobilAvanza.setBesarSilinder(1800);

        System.out.println("Mobil avanza memiliki ukuran silinder sebesar: " +
                mobilAvanza.getBesarSilinder() + " cc");
        System.out.println("Mobil avanza memiliki jumlah roda: " +
                mobilAvanza.getRoda());

        mobilAvanza.doMelaju(125);

        String belok = mobilAvanza.doBelok("kanan");
        System.out.println(belok);

        String tahunPembuatan = mobilAvanza.tahunPembuatan("2021");
        System.out.println(tahunPembuatan);

        String tipeMobil = mobilAvanza.tipe("SUV");
        System.out.println(tipeMobil);

        // Runtime RT = Runtime.getRuntime();
        // System.out.println("Jumlah Memori Awal: " + RT.totalMemory());

        // GarbaceCollector objek1 = new GarbaceCollector();
        // GarbaceCollector objek2 = new GarbaceCollector();
        // GarbaceCollector objek3 = objek1;

        // objek1 = null;
        // objek2 = null;
        // System.out.println("Jumlah Memori Yeng Tersedia Sebelum di GC: " +
        // RT.freeMemory());
        // System.gc();
        // System.out.println("===========================================");
        // System.out.println("Jumlah Memori Yeng Tersedia Setelah di GC: " +
        // RT.freeMemory());
    }
}
