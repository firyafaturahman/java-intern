package com.pbo;

public class GarbaceCollector {
  public class GarbaceCollectorClass {
    // Method ini akan ditampilkan jika Garbage Collection bekerja
    public void finalize() {
      System.out.println("Objek Yang Tidak Terpakai Sudah Dibersihkan:");
    }
  }
}
