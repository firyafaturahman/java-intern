package com.pbo;

public abstract class Keterangan {
  public abstract String tipe(String tipe);

  public abstract String tahunPembuatan(String tahun);
}
