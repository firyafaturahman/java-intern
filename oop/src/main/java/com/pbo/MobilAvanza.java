package com.pbo;

public class MobilAvanza extends Mobil {

  private String warna;

  MobilAvanza() {
  }

  MobilAvanza(String warna, int roda, double besarSilinder) {
    super(warna, roda, besarSilinder);
  }

  public String getWarna() {
    return this.warna;
  }

  public void setWarna(String warna) {
    this.warna = warna;
  }

  public void doMelaju(double kecepatan) {
    System.out.println("Mobil melaju dengan kecepatan: " + kecepatan + " km/jam");
  }

  public String doBelok(String arah) {
    return "Mobil berbelok ke arah: " + arah;
  }

  @Override
  public double getBesarSilinder() {
    return super.getBesarSilinder();
  }

  @Override
  public int getRoda() {
    return super.getRoda();
  }

  @Override
  public void setBesarSilinder(double silinder) {
    super.setBesarSilinder(silinder);
  }

  @Override
  public void setRoda(int roda) {
    super.setRoda(roda);
  }

  @Override
  public String tahunPembuatan(String tahun) {

    return super.tahunPembuatan(tahun);
  }

  @Override
  public String tipe(String tipe) {

    return super.tipe(tipe);
  }

}
